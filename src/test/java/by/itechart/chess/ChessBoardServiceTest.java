package by.itechart.chess;

import by.itechart.chess.domain.*;
import by.itechart.chess.service.ChessBoardService;
import by.itechart.chess.service.ServiceFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ChessBoardServiceTest {
    private static ServiceFactory factory;
    private ChessBoardService chessBoardService;

    @BeforeClass
    public static void getFactoryInstance() {
        factory = ServiceFactory.getInstance();
    }

    @Before
    public void getChessBoardService() {
        chessBoardService = factory.getChessBoardService();
    }

    @Test
    public void noEmptyChessBoardTest() {
        ChessBoard chessBoard = chessBoardService.createChessBoard();
        Assert.assertFalse(chessBoard.getBoard()[0][0].isEmpty());
    }

    @Test
    public void rightPieceLocationTest() {
        ChessBoard chessBoard = chessBoardService.createChessBoard();
        Assert.assertEquals(chessBoard.getBoard()[7][4].getPiece(), new King(Color.WHITE));
        Assert.assertEquals(chessBoard.getBoard()[0][4].getPiece(), new King(Color.BLACK));
        Assert.assertEquals(chessBoard.getBoard()[0][0].getPiece(), new Castle(Color.BLACK));
        Assert.assertEquals(chessBoard.getBoard()[0][1].getPiece(), new Knight(Color.BLACK));
        Assert.assertEquals(chessBoard.getBoard()[0][2].getPiece(), new Bishop(Color.BLACK));
        Assert.assertEquals(chessBoard.getBoard()[0][3].getPiece(), new Queen(Color.BLACK));


    }

}
