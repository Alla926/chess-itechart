package by.itechart.chess.domain;

import java.io.Serializable;
import java.util.Objects;

public class Move implements Serializable {
    private final int x;
    private final int y;
    private boolean firstMoveOnly;
    private boolean onTakeOnly;

    public Move(int x, int y, boolean firstMoveOnly, boolean onTakeOnly) {
        this. x = x;
        this. y = y;
        this.firstMoveOnly = firstMoveOnly;
        this.onTakeOnly = onTakeOnly;
    }

    public Move(int x, int y) {
        this. x = x;
        this. y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isFirstMoveOnly() {
        return firstMoveOnly;
    }

    public boolean isOnTakeOnly() {
        return onTakeOnly;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return x == move.x &&
                y == move.y &&
                firstMoveOnly == move.firstMoveOnly &&
                onTakeOnly == move.onTakeOnly;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, firstMoveOnly, onTakeOnly);
    }

    @Override
    public String toString() {
        return "Move{" +
                "x=" + x +
                ", y=" + y +
                ", firstMoveOnly=" + firstMoveOnly +
                ", onTakeOnly=" + onTakeOnly +
                '}';
    }
}
