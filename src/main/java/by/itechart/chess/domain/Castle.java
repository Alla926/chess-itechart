package by.itechart.chess.domain;

public class Castle extends Piece {
    public Castle(Color color) {
        super(PieceType.CASTLE, color);
        moves = initializeMoves();
        repeatableMoves = true;
    }

    @Override
    protected Move[] initializeMoves() {
            return new Move[]{
                    new Move(0, 1),
                    new Move(1, 0),
                    new Move(0, -1),
                    new Move(-1, 0)};

    }
}
