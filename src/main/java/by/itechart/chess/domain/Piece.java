package by.itechart.chess.domain;

import java.io.Serializable;
import java.util.Objects;
/** Represents a piece.
 * @author Ala Hryntsevich
 */
public abstract class Piece implements Serializable {
    private PieceType type;
    protected Color color;
    Move[] moves;
    boolean repeatableMoves;


    protected abstract Move[] initializeMoves();

    public Piece() {
    }

    public Piece(PieceType type, Color color, Move[] moves) {
        this.type = type;
        this.color = color;
        this.moves = moves;
    }

    public Piece(PieceType type, Color color) {
        this.type = type;
        this.color = color;
    }

    /** Gets an opportunity of repeatable moves.
     * @return A boolean value if a piece can move throw one space or many
     */
    public boolean hasRepeatableMoves(){ return repeatableMoves; }

    /** Gets a color of an opponent
     * @return a Color value of another color (black-white, white-black)
     * @param color current color
     */
    public static Color opponent(Color color) {
        return (color == Color.BLACK) ? Color.WHITE : Color.BLACK;
    }

    /** Gets a type of piece
     * @return PieceType value of piece (etc. Queen - Q)
     */
    public PieceType getType() {
        return type;
    }

    /** Sets a type of piece
     */
    public void setType(PieceType type) {
        this.type = type;
    }

    /** Gets a color of piece
     * @return PieceType value of piece (etc. Queen - Q)
     */
    public Color getColor() {
        return color;
    }


    public void setColor(Color color) {
        this.color = color;
    }
    /** Gets an array of moves
     * @return Move[] array of possible moves
     */
    public Move[] getMoves() {
        return moves;
    }

    public void setMoves(Move[] moves) {
        this.moves = moves;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        return type == piece.type &&
                color == piece.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, color);
    }

    @Override
    public String toString() {
        return "Piece{" +
                "type=" + type +
                ", color=" + color +
                '}';
    }
}
