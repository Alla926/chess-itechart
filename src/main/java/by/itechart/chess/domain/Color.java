package by.itechart.chess.domain;

public enum Color {
    WHITE, BLACK
}
