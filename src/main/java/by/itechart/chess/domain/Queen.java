package by.itechart.chess.domain;

public class Queen extends Piece {
    public Queen(Color color) {
        super(PieceType.QUEEN, color);
        moves = initializeMoves();
        repeatableMoves = true;
    }

    @Override
    protected Move[] initializeMoves() {
        return new Move[]{
                new Move(-1, 0),
                new Move(0,-1),
                new Move (1, 1),
                new Move(-1, 1),
                new Move(1, -1),
                new Move (1, 0),
                new Move(0, 1),
                new Move(-1,-1)
        };
    }


}
