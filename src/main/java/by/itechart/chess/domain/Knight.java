package by.itechart.chess.domain;

public class Knight extends Piece {
    public Knight(Color color) {
        super(PieceType.KNIGHT, color);
        moves = initializeMoves();
    }

    @Override
    protected Move[] initializeMoves() {
        return new Move[]{
                new Move(2, 1),
                new Move(2, -1),
                new Move(-2, 1),
                new Move(-2, -1),
                new Move(1, 2),
                new Move(-1, 2),
                new Move(-1, -2)

    };
    }
}