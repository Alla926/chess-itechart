package by.itechart.chess.domain;

public enum  PieceType {
    BISHOP("B"),
    CASTLE("C"),
    KING("K"),
    KNIGHT("N"),
    PAWN("P"),
    QUEEN("Q");

    private String name;

    PieceType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
