package by.itechart.chess.domain;

import java.io.Serializable;
import java.util.Objects;

public class Tile implements Serializable {
    private Color color;
    private int row;
    private int column;
    private Piece piece;

    public Tile() {
    }

    public Tile(Color color, int row, int column) {
        this.color = color;
        this.row = row;
        this.column = column;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public boolean isEmpty(){
        return piece == null;
    }

    public void empty() {
        piece = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile tile = (Tile) o;
        return row == tile.row &&
                column == tile.column &&
                color == tile.color &&
                piece.equals(tile.piece);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, row, column, piece);
    }

    @Override
    public String toString() {
        return "Tile{" +
                "color=" + color +
                ", row=" + row +
                ", column=" + column +
                ", piece=" + piece +
                '}';
    }
}
