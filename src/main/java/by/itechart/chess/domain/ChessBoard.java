package by.itechart.chess.domain;


import java.util.Arrays;

public class ChessBoard {
    private final Tile[][] board;

    public ChessBoard(Tile[][] board) {
        this.board = board;
    }

    public Tile[][] getBoard() {
        return board;
    }

    public Tile getTileFromTuple(Tuple tuple) {
        if (tuple.getX() < 0 || tuple.getX() > 7 || tuple.getY() < 0 || tuple.getY() > 7) {
            return board[0][0];
        }
        return board[tuple.getY()][tuple.getX()];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChessBoard that = (ChessBoard) o;
        return Arrays.equals(board, that.board);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(board);
    }

    @Override
    public String toString() {
        return "ChessBoard{" +
                "board=" + Arrays.toString(board) +
                '}';
    }
}
