package by.itechart.chess.domain;

public class Pawn extends Piece {
    public Pawn(Color color) {
        super(PieceType.PAWN, color);
        moves = initializeMoves();
    }

    @Override
    protected Move[] initializeMoves() {
        if (color == Color.BLACK) {
            return new Move[]{
                    new Move(0, 1, false, false),
                    new Move(1, 1, false, true),
                    new Move(0, 2, true, false),
                    new Move(-1, 1, false, true)};
        } else {
            return new Move[]{
                    new Move(0, -1, false, false),
                    new Move(1, -1, false, true),
                    new Move(0, -2, true, false),
                    new Move(-1, -1, false, true)
            };
        }
    }
}
