package by.itechart.chess.domain;

public class King extends Piece {
    public King(Color color) {
        super(PieceType.KING, color);
        moves = initializeMoves();

    }

    @Override
    protected Move[] initializeMoves() {
        return new Move[]{
                new Move (1, 0),
                new Move(0, 1),
                new Move(-1, 0),
                new Move(0,-1),
                new Move (1, 1),
                new Move(-1, 1),
                new Move(1, -1),
                new Move(-1,-1)
        };
    }


}
