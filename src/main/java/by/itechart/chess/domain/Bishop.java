package by.itechart.chess.domain;

public class Bishop extends Piece {
    private static final double BISHOP_WEIGHT = 3.0;
    public Bishop(Color color) {
        super(PieceType.BISHOP, color);
        moves = initializeMoves();
        repeatableMoves = true;
    }

    @Override
    protected Move[] initializeMoves() {
        return new Move[]{
                new Move (1, 1),
                new Move(1, 1),
                new Move(-1, 1),
                new Move(-1,-1)
        };
    }
}
