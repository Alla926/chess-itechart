package by.itechart.chess;

import by.itechart.chess.domain.Tuple;
import by.itechart.chess.domain.ChessBoard;
import by.itechart.chess.service.GameService;
import by.itechart.chess.service.ServiceFactory;
import by.itechart.chess.view.BoardDisplay;
import by.itechart.chess.view.InputHandler;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        InputHandler handler = new InputHandler();
        Scanner scanner = new Scanner(System.in);
        GameService game = ServiceFactory.getInstance().getGameService();
        game.startGame();

        ChessBoard board = game.getBoard();

        BoardDisplay.printBoard(board);


        while (!game.isFinished()) {
            System.out.println("Your move, format A2-A4 ");
            String input = scanner.nextLine();

            if (!handler.isValid(input)) {
                System.out.println("Incorrect input");
            } else {
                Tuple from = handler.getFrom(input);
                Tuple to = handler.getTo(input);

                boolean movePlayed = game.playMove(from, to);
                if (!movePlayed)
                    System.out.println("Incorrect move");
                else {
                    BoardDisplay.printBoard(game.getBoard());
                }
            }
        }
        scanner.close();
        System.out.println("Game finished");
    }
}
