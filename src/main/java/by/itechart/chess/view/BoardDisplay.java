package by.itechart.chess.view;

import by.itechart.chess.domain.ChessBoard;
import by.itechart.chess.domain.Tile;

import java.util.Arrays;


public class BoardDisplay {
    public static void printBoard(ChessBoard chessBoard){
        Tile[][] board = chessBoard.getBoard();
        System.out.println();
        System.out.println("   A  B  C  D  E  F  G  H");
        for(int i = 0; i < 8; i++) {
            System.out.print("" + (8 - i) + " ");

            for (int j = 0; j < 8; j++){
                if (board[i][j].getPiece() == null) {
                    System.out.print("{ }");
                } else {
                    System.out.print("{"+board[i][j].getPiece().getType().getName()+"}");
                }
            }
            System.out.println(" " + (8 - i) + " ");
        }
        System.out.println("   A  B  C  D  E  F  G  H");
    }
}
