package by.itechart.chess.service;

import by.itechart.chess.domain.ChessBoard;
/** Creates and fills a chess board
 * @author Ala Hryntsevich
 */
public interface ChessBoardService {
    /** create a chess board without pieces
     * @return ChessBoard object with black-white spaces
     */
    ChessBoard createChessBoard();

    /** fill a chess board with pieces
     */
    void fillBoard(ChessBoard board);

}
