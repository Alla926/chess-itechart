package by.itechart.chess.service.impl;

import by.itechart.chess.domain.*;
import by.itechart.chess.service.MoveValidationService;
import by.itechart.chess.service.MovingService;
import by.itechart.chess.service.PieceLocationService;
import by.itechart.chess.service.ServiceFactory;

import java.util.List;


public class MoveValidationServiceImpl implements MoveValidationService {
    @Override
    public boolean isFirstMoveForPawn(Tuple from, ChessBoard board) {
        Tile tile = board.getTileFromTuple(from);
        if (tile.isEmpty() || tile.getPiece().getType() != PieceType.PAWN) {
            return false;
        } else {
            Color color = tile.getPiece().getColor();
            return (color == Color.WHITE)
                    ? from.getY() == 6
                    : from.getY() == 1;
        }
    }

    @Override
    public boolean isValidMoveForPieceNonRepeatable(Tuple from, Tuple to, ChessBoard chessBoard) {
        Piece fromPiece = chessBoard.getTileFromTuple(from).getPiece();
        Move[] validMoves = fromPiece.getMoves();
        Tile toTile = chessBoard.getTileFromTuple(to);

        int xMove = to.getX() - from.getX();
        int yMove = to.getY() - from.getY();


        for (Move move : validMoves) {
            if (move.getX() == xMove && move.getY() == yMove) {
                if (move.isOnTakeOnly()){
                    if (toTile.isEmpty()) {
                        return false;
                    }

                    Piece toPiece = toTile.getPiece();
                    return fromPiece.getColor() != toPiece.getColor();

                } else if (move.isFirstMoveOnly()) {
                    return toTile.isEmpty() && isFirstMoveForPawn(from, chessBoard);
                } else {
                    return toTile.isEmpty();
                }
            }
        }
        return false;
    }

    @Override
    public boolean isValidMoveForPieceRepeatable(Tuple from, Tuple to, ChessBoard chessBoard, Color currentPlayer) {
        Piece fromPiece = chessBoard.getTileFromTuple(from).getPiece();
        Move[] validMoves = fromPiece.getMoves();

        int xMove = to.getX() - from.getX();
        int yMove = to.getY() - from.getY();

        for(int i = 1; i <= 7; i++){
            for(Move move : validMoves) {
                if (move.getX() * i != xMove || move.getY() * i != yMove) {
                    continue;
                }

                for (int j = 1; j <= i; j++){
                    Tuple tupleMove = new Tuple(from.getX() + move.getX() * j, from.getY() +move.getY() * j);
                    Tile tile = chessBoard.getTileFromTuple(tupleMove);

                    if (j != i && !tile.isEmpty()) {
                        return false;
                    }

                    if (j == i && (tile.isEmpty() || tile.getPiece().getColor() != currentPlayer)) {
                        return true;
                    }
                }

            }
        }
        return false;
    }

    @Override
    public boolean isValidMoveForPiece(Tuple from, Tuple to, ChessBoard chessBoard, Color currentPlayer) {
        Piece fromPiece = chessBoard.getTileFromTuple(from).getPiece();
        boolean repeatableMoves = fromPiece.hasRepeatableMoves();

        return repeatableMoves
                ? isValidMoveForPieceRepeatable(from, to, chessBoard, currentPlayer)
                : isValidMoveForPieceNonRepeatable(from, to, chessBoard);
    }

    @Override
    public boolean isValidMove(Tuple from, Tuple to, boolean hypothetical, ChessBoard chessBoard, Color currentPlayer) {
        Tile fromTile = chessBoard.getTileFromTuple(from);
        Tile toTile = chessBoard.getTileFromTuple(to);
        Piece fromPiece = fromTile.getPiece();
        Piece toPiece = toTile.getPiece();

        if (fromPiece == null){
            return false;
        } else if (fromPiece.getColor() != currentPlayer) {
            return false;
        } else if (toPiece != null && toPiece.getColor() == currentPlayer) {
            return false;
        } else if (isValidMoveForPiece(from, to, chessBoard, currentPlayer)){
            if(hypothetical) return true;

            toTile.setPiece(fromPiece);
            fromTile.empty();
            if (isKingCheck(currentPlayer, chessBoard)){
                toTile.setPiece(toPiece);
                fromTile.setPiece(fromPiece);
                return false;
            }

            toTile.setPiece(toPiece);
            fromTile.setPiece(fromPiece);

            return true;
        }
        return false;
    }

    @Override
    public boolean isColorCheckMate(Color color, ChessBoard chessBoard) {
        if(!isKingCheck(color, chessBoard)) return false;
        return !isCheckPreventable(color, chessBoard);
    }

    private boolean isCheckPreventable(Color color, ChessBoard chessBoard){
        boolean canPreventCheck = false;
        PieceLocationService pieceLocationService = ServiceFactory.getInstance().getPieceLocationService();
        MovingService movingService = ServiceFactory.getInstance().getMovingService();
        List<Tuple> locations = pieceLocationService.getAllPiecesLocationByColor(chessBoard, color);

        for(Tuple location : locations){
            Tile fromTile = chessBoard.getTileFromTuple(location);
            Piece piece = fromTile.getPiece();
            List<Tuple> possibleMoves = movingService.getValidMovesForPiece(piece, location, chessBoard, color);

            for(Tuple newLocation : possibleMoves){
                Tile toTile = chessBoard.getTileFromTuple(newLocation);
                Piece toPiece = toTile.getPiece();

                toTile.setPiece(piece);
                fromTile.empty();

                if (!isKingCheck(color, chessBoard)){
                    canPreventCheck = true;
                }
                toTile.setPiece(toPiece);
                fromTile.setPiece(piece);
                if(canPreventCheck){
                    return true;
                }
            }
        }

        return false;
    }


    private boolean isKingCheck(Color kingColor, ChessBoard chessBoard){
        PieceLocationService pieceLocationService = ServiceFactory.getInstance().getPieceLocationService();

        Tuple kingLocation = pieceLocationService.getKingLocationByColor(chessBoard, kingColor);
        return canOpponentTakeLocation(kingLocation, kingColor, chessBoard);
    }

    private boolean canOpponentTakeLocation(Tuple location, Color color, ChessBoard chessBoard){
        PieceLocationService pieceLocationService = ServiceFactory.getInstance().getPieceLocationService();

        Color opponentColor = Piece.opponent(color);
        List<Tuple> piecesLocation = pieceLocationService.getAllPiecesLocationByColor(chessBoard, opponentColor);

        return piecesLocation.stream()
                .anyMatch(fromTuple -> isValidMove(fromTuple, location, true, chessBoard, color));
    }


}
