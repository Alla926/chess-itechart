package by.itechart.chess.service.impl;

import by.itechart.chess.domain.*;
import by.itechart.chess.service.MoveValidationService;
import by.itechart.chess.service.MovingService;
import by.itechart.chess.service.ServiceFactory;

import java.util.ArrayList;
import java.util.List;

public class MovingServiceImpl implements MovingService {

    @Override
    public List<Tuple> getMovesNonRepeatable(Piece piece, Tuple currentLocation, ChessBoard chessBoard, Color currentPlayer) {
        Move[] moves = piece.getMoves();
        List<Tuple> possibleMoves = new ArrayList<>();

        for(Move move: moves){
            int currentX = currentLocation.getX();
            int currentY = currentLocation.getY();
            int newX = currentX + move.getX();
            int newY = currentY + move.getY();
            if (newX < 0 || newX > 7 || newY < 0 || newY > 7) continue;
            Tuple newLocation = new Tuple(newX,newY);
            MoveValidationService moveValidationService = ServiceFactory.getInstance().getMoveValidationService();
            if (moveValidationService.isValidMoveForPiece(currentLocation, newLocation,chessBoard, currentPlayer)) {
                possibleMoves.add(newLocation);
            }
        }
        return possibleMoves;
    }

    @Override
    public List<Tuple> getMovesRepeatable(Piece piece, Tuple currentLocation, ChessBoard chessBoard) {
        Move[] moves = piece.getMoves();
        ArrayList<Tuple> possibleMoves = new ArrayList<>();

        for(Move move: moves){
            for(int i = 1; i < 7; i++){
                int newX = currentLocation.getX() + move.getX() * i;
                int newY = currentLocation.getY() + move.getY() * i;

                if (newX < 0 || newX > 7 || newY < 0 || newY > 7) {
                    break;
                }

                Tuple toLocation = new Tuple(newX, newY);
                Tile tile = chessBoard.getTileFromTuple(toLocation);

                if (tile.isEmpty()) {
                    possibleMoves.add(toLocation);

                } else {
                    if (tile.getPiece().getColor() != piece.getColor()) {
                        possibleMoves.add(toLocation);
                    }
                    break;
                }
            }
        }
        return possibleMoves;
    }

    @Override
    public List<Tuple> getValidMovesForPiece(Piece piece, Tuple currentLocation, ChessBoard chessBoard, Color currentPlayer) {
        return piece.hasRepeatableMoves()
                ? getMovesRepeatable(piece, currentLocation, chessBoard)
                : getMovesNonRepeatable(piece, currentLocation, chessBoard, currentPlayer);
    }


}
