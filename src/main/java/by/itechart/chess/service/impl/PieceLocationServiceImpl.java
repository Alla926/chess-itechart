package by.itechart.chess.service.impl;

import by.itechart.chess.domain.*;
import by.itechart.chess.service.PieceLocationService;

import java.util.ArrayList;
import java.util.List;

public class PieceLocationServiceImpl implements PieceLocationService {
    private static final int BOARD_SIZE = 8;

    @Override
    public Tuple getKingLocationByColor(ChessBoard chessBoard, Color color) {
        Tile[][] board = chessBoard.getBoard();
        Tuple location = new Tuple(-1,-1);
        for (int x = 0; x < BOARD_SIZE; x++){
            for (int y = 0; y < BOARD_SIZE ; y++){
                if (!board[y][x].isEmpty()) {
                    Piece piece = board[y][x].getPiece();
                    if (piece.getColor() == color && piece instanceof King){
                        location = new Tuple(x, y);
                    }
                }
            }
        }

        return location;
    }

    @Override
    public List<Tuple> getAllPiecesLocationByColor(ChessBoard chessBoard, Color color) {
        Tile[][] board = chessBoard.getBoard();
        List<Tuple> locations = new ArrayList<>();
        for (int x = 0; x < BOARD_SIZE; x++){
            for (int y = 0; y < BOARD_SIZE; y++){
                if(!board[y][x].isEmpty() && board[y][x].getPiece().getColor() == color)
                    locations.add(new Tuple(x,y));
            }
        }
        return locations;
    }
}
