package by.itechart.chess.service.impl;

import by.itechart.chess.domain.*;
import by.itechart.chess.service.ChessBoardService;
import by.itechart.chess.service.GameService;
import by.itechart.chess.service.MoveValidationService;
import by.itechart.chess.service.ServiceFactory;

public class GameServiceImpl implements GameService {
    private ChessBoard board;
    private boolean isFinished;
    private Color currentPlayer;
    private MoveValidationService moveValidationService;

    @Override
    public void startGame() {
        moveValidationService = ServiceFactory.getInstance().getMoveValidationService();
        ChessBoardService chessBoardService = ServiceFactory.getInstance().getChessBoardService();
        board = chessBoardService.createChessBoard();
        chessBoardService.fillBoard(board);
        currentPlayer = Color.WHITE;
        isFinished = false;
    }

    @Override
    public boolean isFinished() {
        return isFinished;
    }

    @Override
    public ChessBoard getBoard() {
        return board;
    }

    @Override
    public boolean playMove(Tuple from, Tuple to) {
        if(moveValidationService.isValidMove(from, to, false, board, currentPlayer)) {

            isFinished = moveValidationService.isColorCheckMate(Piece.opponent(currentPlayer), board);

            Tile fromTile = board.getBoard()[from.getY()][from.getX()];
            Piece pieceToMove = fromTile.getPiece();

            Tile toTile = board.getBoard()[to.getY()][to.getX()];
            toTile.setPiece(pieceToMove);

            fromTile.empty();
            endTurn();

            return true;
        }

        return false;
    }

    private void endTurn(){
        currentPlayer = (currentPlayer == Color.WHITE)
                ? Color.BLACK
                : Color.WHITE;
    }
}
