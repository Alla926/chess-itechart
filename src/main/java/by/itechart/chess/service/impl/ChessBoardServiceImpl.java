package by.itechart.chess.service.impl;

import by.itechart.chess.domain.*;
import by.itechart.chess.service.ChessBoardService;

public class ChessBoardServiceImpl implements ChessBoardService {
    private static final int BOARD_SIZE = 8;

    @Override
    public ChessBoard createChessBoard() {
        Tile[][] board = new Tile[BOARD_SIZE][BOARD_SIZE];
        ChessBoard chessBoard = new ChessBoard(board);

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (j % 2 + i == 0) {
                    board[i][j] = new Tile(Color.BLACK, i, j);
                } else {
                    board[i][j] = new Tile(Color.WHITE, i, j);
                }
            }
        }

        fillBoard(chessBoard);

        return chessBoard;
    }

    @Override
    public void fillBoard(ChessBoard chessBoard) {
        Tile[][] board = chessBoard.getBoard();
        for (int i = 0; i < BOARD_SIZE; i++) {
            board[1][i].setPiece(new Pawn(Color.BLACK));
            board[6][i].setPiece(new Pawn(Color.WHITE));
        }

        board[0][0].setPiece(new Castle(Color.BLACK));
        board[0][7].setPiece(new Castle(Color.BLACK));
        board[7][0].setPiece(new Castle(Color.WHITE));
        board[7][7].setPiece(new Castle(Color.WHITE));

        board[0][1].setPiece(new Knight(Color.BLACK));
        board[0][6].setPiece(new Knight(Color.BLACK));
        board[7][1].setPiece(new Knight(Color.WHITE));
        board[7][6].setPiece(new Knight(Color.WHITE));

        board[0][2].setPiece(new Bishop(Color.BLACK));
        board[0][5].setPiece(new Bishop(Color.BLACK));
        board[7][2].setPiece(new Bishop(Color.WHITE));
        board[7][5].setPiece(new Bishop(Color.WHITE));

        board[0][3].setPiece(new Queen(Color.BLACK));
        board[7][3].setPiece(new Queen(Color.WHITE));

        board[0][4].setPiece(new King(Color.BLACK));
        board[7][4].setPiece(new King(Color.WHITE));
    }
}
