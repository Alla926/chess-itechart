package by.itechart.chess.service;

import by.itechart.chess.domain.ChessBoard;
import by.itechart.chess.domain.Color;
import by.itechart.chess.domain.Tuple;

import java.util.List;

public interface PieceLocationService {
    Tuple getKingLocationByColor(ChessBoard chessBoard, Color color);

    List<Tuple> getAllPiecesLocationByColor(ChessBoard chessBoard, Color color);
}
