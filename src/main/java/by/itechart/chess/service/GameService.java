package by.itechart.chess.service;

import by.itechart.chess.domain.ChessBoard;
import by.itechart.chess.domain.Tuple;

public interface GameService {
    void startGame();

    boolean isFinished();

    ChessBoard getBoard();

    boolean playMove(Tuple from, Tuple to);
}
