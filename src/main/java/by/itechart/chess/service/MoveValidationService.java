package by.itechart.chess.service;

import by.itechart.chess.domain.ChessBoard;
import by.itechart.chess.domain.Color;
import by.itechart.chess.domain.Piece;
import by.itechart.chess.domain.Tuple;

import java.util.List;

public interface MoveValidationService {
    boolean isFirstMoveForPawn(Tuple from, ChessBoard board);

    boolean isValidMoveForPieceNonRepeatable(Tuple from, Tuple to, ChessBoard chessBoard);

    boolean isValidMoveForPieceRepeatable(Tuple from, Tuple to, ChessBoard chessBoard, Color currentPlayer);

    boolean isValidMoveForPiece(Tuple from, Tuple to, ChessBoard chessBoard, Color currentPlayer);

    boolean isValidMove(Tuple from, Tuple to, boolean hypothetical, ChessBoard chessBoard, Color currentPlayer);

    boolean isColorCheckMate(Color color, ChessBoard chessBoard);
}
