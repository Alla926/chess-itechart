package by.itechart.chess.service;

import by.itechart.chess.domain.ChessBoard;
import by.itechart.chess.domain.Color;
import by.itechart.chess.domain.Piece;
import by.itechart.chess.domain.Tuple;

import java.util.List;

public interface MovingService {
    List<Tuple> getMovesNonRepeatable(Piece piece, Tuple currentLocation, ChessBoard chessBoard, Color currentPlayer);

    List<Tuple> getMovesRepeatable(Piece piece, Tuple currentLocation, ChessBoard chessBoard);

    List<Tuple> getValidMovesForPiece(Piece piece, Tuple currentLocation, ChessBoard chessBoard, Color currentPlayer);
}
