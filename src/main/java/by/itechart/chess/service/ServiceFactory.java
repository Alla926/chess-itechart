package by.itechart.chess.service;

import by.itechart.chess.service.impl.*;

public class ServiceFactory {
    private static final ServiceFactory instance = new ServiceFactory();
    private PieceLocationService pieceLocationService = new PieceLocationServiceImpl();
    private MovingService movingService = new MovingServiceImpl();
    private MoveValidationService moveValidationService = new MoveValidationServiceImpl();
    private GameService gameService = new GameServiceImpl();
    private ChessBoardService chessBoardService = new ChessBoardServiceImpl();

    private ServiceFactory() {}

    public static ServiceFactory getInstance() {
        return instance;
    }

    public PieceLocationService getPieceLocationService() {
        return pieceLocationService;
    }

    public MovingService getMovingService() {
        return movingService;
    }

    public MoveValidationService getMoveValidationService() {
        return moveValidationService;
    }

    public GameService getGameService() {
        return gameService;
    }

    public ChessBoardService getChessBoardService() {
        return chessBoardService;
    }
}
