I did not have time to do everything. I tried to do more. It was an interesting task. I love playing chess.

I did not have time to do a full refactoring and testing. If there was more time, more patterns could be used (now there is a factory, singleton).

Documentation is provided in Piece and ChessBoardService. Only one service has been tested (ChessBoardService).

I really hope for a positive review. Looking forward to it.